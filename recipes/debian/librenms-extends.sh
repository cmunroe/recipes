#!/bin/bash

###################################################
#
#  git: https://gitlab.com/cmunroe/recipes
# 
#  By Cameron Munroe
#  
###################################################


## =========================================
##         LibreNMS Exentenders
## =========================================

while true; do
    PS3="Please enter your choice: "
    options=("Chronyd" "Docker" "Entropy" "MailCow" "NTP Server" "NUT" "Postfix" "Proxmox" "ZFS" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "Chronyd")
                apt install -y  sudo

                wget https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/chrony -O /etc/snmp/chrony
                chmod +x /etc/snmp/chrony

                echo extend chronyd /usr/bin/sudo /etc/snmp/chrony >> /etc/snmp/snmpd.conf
                echo Debian-snmp ALL=\(ALL\) NOPASSWD: /etc/snmp/chrony >> /etc/sudoers.d/lnms-chronyd

                systemctl restart snmpd
                
                break
                ;;
            "Docker")
                apt install -y python3-dateutil sudo

                wget https://github.com/librenms/librenms-agent/raw/master/snmp/docker-stats.py -O /etc/snmp/docker-stats.py
                chmod +x /etc/snmp/docker-stats.py

                echo extend docker /usr/bin/sudo /etc/snmp/docker-stats.py >> /etc/snmp/snmpd.conf
                echo Debian-snmp ALL=\(ALL\) NOPASSWD: /etc/snmp/docker-stats.py >> /etc/sudoers.d/lnms-docker

                systemctl restart snmpd
                
                break
                ;;
            "Entropy")
                wget https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/entropy.sh -O /etc/snmp/entropy.sh
                chmod +x /etc/snmp/entropy.sh
                echo extend entropy /etc/snmp/entropy.sh >> /etc/snmp/snmpd.conf
                systemctl restart snmpd
                break
                ;;
            "MailCow")
                apt install pflogsumm -y
                usermod -aG docker Debian-snmp
                wget https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/mailcow-dockerized-postfix -O /etc/snmp/mailcow-dockerized-postfix
                chmod +x /etc/snmp/mailcow-dockerized-postfix
                echo extend mailcow-postfix /etc/snmp/mailcow-dockerized-postfix >> /etc/snmp/snmpd.conf
                systemctl restart snmpd
                break
                ;;
            "NTP Server")
                wget https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/ntp-server.sh -O /etc/snmp/ntp-server.sh
                chmod +x /etc/snmp/ntp-server.sh
                echo extend ntp-server /etc/snmp/ntp-server.sh >> /etc/snmp/snmpd.conf
                systemctl restart snmpd
                break
                ;;
            "NUT")
                wget https://github.com/librenms/librenms-agent/raw/master/snmp/ups-nut.sh -O /etc/snmp/ups-nut.sh
                chmod +x /etc/snmp/ups-nut.sh
                read -p "Enter NUT UPS Name: " UPSNUTNAME
                echo extend ups-nut /etc/snmp/ups-nut.sh ${UPSNUTNAME} >> /etc/snmp/snmpd.conf
                systemctl restart snmpd
                break
                ;;
            "Postfix")
                apt update
                apt install -y pflogsumm sudo 

                wget https://github.com/librenms/librenms-agent/raw/master/snmp/postfix-queues -O /etc/snmp/postfix-queues
                wget https://github.com/librenms/librenms-agent/raw/master/snmp/postfixdetailed -O /etc/snmp/postfixdetailed
                chmod +x /etc/snmp/postfixdetailed 
                chmod +x /etc/snmp/postfix-queues

                sed -i 's/\/var\/log\/maillog/\/var\/log\/mail.log/g' /etc/snmp/postfixdetailed

                echo Debian-snmp ALL=\(ALL\) NOPASSWD: /etc/snmp/postfix-queues >> /etc/sudoers.d/lnms-postfix
                echo Debian-snmp ALL=\(ALL\) NOPASSWD: /etc/snmp/postfixdetailed >> /etc/sudoers.d/lnms-postfix

                echo extend mailq /usr/bin/sudo /etc/snmp/postfix-queues >> /etc/snmp/snmpd.conf
                echo extend postfixdetailed /usr/bin/sudo /etc/snmp/postfixdetailed >> /etc/snmp/snmpd.conf

                systemctl restart snmpd

                break
                ;;
            "Proxmox")
                #install requirements
                apt update
                apt install libpve-apiclient-perl sudo -y

                wget https://raw.githubusercontent.com/librenms/librenms-agent/master/agent-local/proxmox -O /etc/snmp/proxmox
                chmod +x /etc/snmp/proxmox

                echo extend proxmox /usr/bin/sudo /etc/snmp/proxmox >> /etc/snmp/snmpd.conf
                echo Debian-snmp ALL=\(ALL\) NOPASSWD: /etc/snmp/proxmox >> /etc/sudoers.d/lnms-proxmox

                systemctl restart snmpd

                break
                ;;
            "ZFS")
                #install requirements
                apt update
                apt-get install -y cpanminus zlib1g-dev gcc sudo -y
                cpanm Mime::Base64 JSON Gzip::Faster

                wget https://github.com/librenms/librenms-agent/raw/master/snmp/zfs -O /etc/snmp/zfs
                chmod +x /etc/snmp/zfs

                echo extend zfs /etc/snmp/zfs >> /etc/snmp/snmpd.conf

                systemctl restart snmpd

                break
                ;;
            "Quit")
                break 2
                ;;
        esac
    done
done
