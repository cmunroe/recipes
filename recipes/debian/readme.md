# Debian

Should work with:

* Debian 
* Ubuntu

### Docker
```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/docker.sh | bash
```

### FSTRIM

### IPTables

### Librenms-Extends
```
bash <(wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/librenms-extends.sh)
```


### SNMPD V3 (Deb)

```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/snmpd-v3-install.sh | bash
```

### SNMPD Installer (Deb)

```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/snmpd-install-prompt.sh | bash
```

### SNMPD Installer Hashed (Deb)

```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/snmpd-install.sh | bash
```

### Unattended-upgrades

```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/unattended-upgrades.sh | bash
```

### VM Tools


