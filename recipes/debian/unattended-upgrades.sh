#!/bin/bash

###################################################
#
#  git: https://gitlab.com/cmunroe/recipes
# 
#  By Cameron Munroe 
#  
###################################################

echo "
###############################################
#
#        Unattended-Upgrades Install!
#
###############################################
"


apt-get update

apt-get install apt-listchanges apt-config-auto-update -y 

echo unattended-upgrades unattended-upgrades/enable_auto_updates boolean true | debconf-set-selections
apt-get install unattended-upgrades -y 
