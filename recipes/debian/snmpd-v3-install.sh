#!/bin/bash

###################################################
#
#  git: https://gitlab.com/cmunroe/recipes
# 
#  By Cameron Munroe
#  
###################################################

echo "
###############################################
#
#        Installing SNMPD!
#
###############################################
"

#read -p "Rack / Server ID? " rackSNMPD </dev/tty
#wait
#read -p "Room / Virtualization Type? " roomSNMPD </dev/tty
#wait
#read -p "Building / Hosting Company? " buildingSNMPD </dev/tty
#wait
#read -p "City? " citySNMPD </dev/tty
#wait
#read -p "Country? " countrySNMPD </dev/tty
#wait
#read -p "GPS Latitude? " gpsXSNMPD </dev/tty
#wait
#read -p "GPS Longitude? " gpsYSNMPD </dev/tty
#wait
read -p "Device Owners Name? " nameSNMPD </dev/tty
wait
read -p "Email? " emailSNMPD </dev/tty
wait

citySNMPD=`wget -O- https://ipapi.co/city/`
countrySNMPD=`wget -O- https://ipapi.co/country/`
gpsSNMPD=`wget -O- https://ipapi.co/latlong`


cd /tmp
wget https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/distro
wait
mv /tmp/distro /usr/bin/distro
chmod +x /usr/bin/distro
wait

apt-get update
wait
apt-get install snmpd -y
wait

USER=nms
AUTHPASS=`openssl rand -base64 24`
PRIVPASS=`openssl rand -base64 24`


wait

echo "agentAddress udp:161,udp6:161" > /etc/snmp/snmpd.conf
echo "createUser ${USER} SHA \"${AUTHPASS}\" AES \"${PRIVPASS}\"" >> /etc/snmp/snmpd.conf
echo "rouser ${USER} priv" >> /etc/snmp/snmpd.conf
echo "" >> /etc/snmp/snmpd.conf
echo "syslocation ${citySNMPD}, ${countrySNMPD} [${gpsSNMPD}]" >> /etc/snmp/snmpd.conf
echo "syscontact ${nameSNMPD} ${emailSNMPD}" >> /etc/snmp/snmpd.conf
echo "" >> /etc/snmp/snmpd.conf
echo "#Distro Detection" >> /etc/snmp/snmpd.conf
echo "extend .1.3.6.1.4.1.2021.7890.1 distro /usr/bin/distro" >> /etc/snmp/snmpd.conf

systemctl restart snmpd


echo "#=================================================="
echo "SNMPD Setup with USER:  ${USER}"
echo "SNMPD Setup with Auth Level:  authPriv"
echo "SNMPD Setup with Auth Pass:  ${AUTHPASS}"
echo "SNMPD Setup with Auth Algo:  SHA"
echo "SNMPD Setup with Priv Pass:  ${PRIVPASS}"
echo "SNMPD Setup with Priv Algo:  AES"
echo "#================================================="

mkdir -p /root/recipes

echo "#==================================================" > /root/recipes/snmpd.community.txt
echo "SNMPD Setup with USER:  ${USER}" >> /root/recipes/snmpd.community.txt
echo "SNMPD Setup with Auth Level:  authPriv" >> /root/recipes/snmpd.community.txt
echo "SNMPD Setup with Auth Pass:  ${AUTHPASS}" >> /root/recipes/snmpd.community.txt
echo "SNMPD Setup with Auth Algo:  SHA" >> /root/recipes/snmpd.community.txt
echo "SNMPD Setup with Priv Pass:  ${PRIVPASS}" >> /root/recipes/snmpd.community.txt
echo "SNMPD Setup with Priv Algo:  AES" >> /root/recipes/snmpd.community.txt
echo "#================================================="
