# Recipes

[Debian Recipes](https://gitlab.com/cmunroe/recipes/-/tree/master/recipes/debian)

[Centos Recipes](https://gitlab.com/cmunroe/recipes/-/tree/master/recipes/centos)


## Debian Common Install Booklet.
This basically removes commonly installed apps for OpenVZ/KVM images, then installs commonly used tools, iptables, snmp, and a few other bits.

```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/books/debian/common-install.sh | bash
```

# Common Recipes    
These are some common recipes I use for when I am doing server stuff. 
They might be useful to you as well.


### SNMPD V3 (Deb)

```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/snmpd-v3-install.sh | bash
```

### Librenms Extends (Deb)
```
bash <(wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/librenms-extends.sh)
```

### Docker
```
wget --no-check-certificate -qO- https://gitlab.com/cmunroe/recipes/raw/master/recipes/debian/docker.sh | bash
```

